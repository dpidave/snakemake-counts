from os.path import join
import glob

configfile: "./config.yaml"

print('###########################################################')
print('########## YOU NEED TO USE CONDA ##########################')
print('# usage: snakemake project_setup --cores 12 --use-conda ###')
print('# usage: snakemake build_index --cores 12 --use-conda #####')
print('# usage: snakemake all --cores 12 --use-conda #############')
print('###########################################################')

# globals
REF = config['genome']
THREADS = config['threads']
INDEX = config['index']
GTF = config['gtf']
ADAPTORS = config['adaptors']
DIRS = ['bams/', 'raw_reads/', 'clean_reads/', 'logs/', 'counts/', 'ref/',
    'tables', 'pre_qc/', 'post_qc/']

# Just target R1 (in this case _1.fastq.gz), the following glob will do:
# raw_reads/L175A_2_1.fastq.gz
# raw_reads/R3SA_1_1.fastq.gz
# returns the following, held in SAMPLES
#['L175A_2', 'R3SA_1']
### CHANGE ###
SAMPLES, = glob_wildcards('raw_reads/{sample}_1.fastq.gz')
READS, = glob_wildcards('raw_reads/{sample}.fastq.gz')

# these patttersn will expand based on sample wildcards collected above
PATTERN_R1 = config['pat_r1']
PATTERN_R2 = config['pat_r2']
PATTERN_CLN_R1 = config['cln_r1']
PATTERN_CLN_R2 = config['cln_r2']

print('using the following R1 and R2 patterns')
print(PATTERN_R1)
print(PATTERN_CLN_R1)
print('using the following sample wildcards')
print(SAMPLES)
print(READS)

rule all:
    input:
        REF, GTF, ADAPTORS, DIRS,
        expand('{INDEX}.1.ht2', INDEX=INDEX),
        expand(join('clean_reads/', PATTERN_CLN_R1), sample=SAMPLES),
        expand('pre_qc/{sample}_fastqc.html', sample=READS),
        expand('post_qc/{sample}.cln_fastqc.html', sample=READS),
        expand('bams/{sample}.bam', sample=SAMPLES),
        expand('bams/{sample}.sbn.bam', sample=SAMPLES),
        expand('counts/{sample}.sbn.counts', sample=SAMPLES)
        #'logs/count_results.log'

#expand('clean_reads/{sample}_R1_001.cln.fastq.gz', sample=SAMPLES),
rule project_setup:
    output: DIRS
    shell:
        "mkdir -p "+' '.join(DIRS)

rule make_index:
    input:
        expand('{REF}',REF=REF)
    output:
        expand('{INDEX}.1.ht2', INDEX=INDEX),
    threads: THREADS
    conda:'envs/myenv.yaml'
    log:
        'logs/index_log.txt'
    shell:
        'hisat2-build -p {threads} {input} $(echo "{input}" | sed -e "s/.fa//") > {log} 2>&1'

rule pre_qc:
    input:
        'raw_reads/{sample}.fastq.gz'
    output:
        'pre_qc/{sample}_fastqc.html'
    threads: THREADS
    shell:
        "fastqc -t {threads} -o pre_qc -f fastq {input}"

rule qc_trim:
    input:
        r1 = join('raw_reads/',PATTERN_R1),
        r2 = join('raw_reads/',PATTERN_R2)
    threads: THREADS
    params:
        minlen=config['minlen'],
        qtrim=config['qtrim'],
        trimq=config['trimq'],
        ktrim=config['ktrim'],
        kwin=config['kwin'],
        mink=config['mink'],
        hdist=config['hdist']
    output:
        r1_out = join('clean_reads/', PATTERN_CLN_R1),
        r2_out = join('clean_reads/', PATTERN_CLN_R2),
    conda:'envs/myenv.yaml'
    log:
        "logs/{sample}.trim.log"
    shell:
        "bbduk.sh in1={input.r1} in2={input.r2} out1={output.r1_out} out2={output.r2_out} "
        "minlen={params.minlen} qtrim={params.qtrim} trimq={params.trimq} "
        "ktrim={params.ktrim} k={params.kwin} mink={params.mink} "
        "ref={ADAPTORS} hdist={params.hdist} #2>&1 | tee -a {log}"

rule post_qc:
    input:
        'clean_reads/{sample}.cln.fastq.gz'
    output:
        'post_qc/{sample}.cln_fastqc.html'
    threads: THREADS
    shell:
        "fastqc -t {threads} -o post_qc -f fastq {input}"

rule aln:
    input:
        ref=expand('{REF}',REF=REF),
        r1 = join('clean_reads/', PATTERN_CLN_R1),
        r2 = join('clean_reads/', PATTERN_CLN_R2)
    log:
        'logs/{sample}.aln_log.txt'
    conda:'envs/myenv.yaml'
    output:
        bam='bams/{sample}.bam',
        splice='ref/{sample}.novel_splices.txt'
    params:
        index=expand('{INDEX}',INDEX=INDEX),
        strand=config['alnstrand']
    threads: THREADS
    shell:
        """
        echo {output.bam} >> {log}
        hisat2 -p {threads} -x {params.index} -q --dta -1 {input.r1} -2 {input.r2} \
            --rna-strandness {params.strand} --novel-splicesite-outfile {output.splice} \
            2>> {log} | samtools sort -@4 - $(echo {output.bam} | sed 's/.bam//g')
        """
# don't delete the pos sorted bam or rerunning will try to regenerate it (ie
# aln again...) 
rule sort_bam:
    input:
        'bams/{sample}.bam'
    output:
        'bams/{sample}.sbn.bam'
    conda:'envs/myenv.yaml'
    shell:
        """
        samtools index {input}
        samtools sort -n {input} $(echo {output} | sed 's/.bam//g')
        """

rule do_counts:
    input:
        'bams/{sample}.sbn.bam'
    output:
        'counts/{sample}.sbn.counts'
    conda:'envs/myenv.yaml'
    log:
        'logs/{sample}_count.log'
    params:
        strand=config['countstrand'],
        countmode=config['mode']
    shell:
        'htseq-count -r name -s {params.strand} -f bam -m {params.countmode} {input} '
        '{GTF} > {output} 2> {log}'

rule log_count_result:
    input:
        expand('counts/{sample}.sbn.counts', sample=SAMPLES)
    output:
        'logs/count_results.log',
    shell:
        'tail -n5 counts/*counts > {output}'

rule make_latex_tables:
    input:
        aln='logs/aln_log.txt',
        qc='logs/trim_log.txt'
    output:
        aln='tables/aln_table.tex',
        qc='tables/qc_table.tex'
    shell:
        """
        python scripts/make_aln_tab.py {input.aln} > {output.aln}
        python scripts/make_qc_tab.py {input.qc} > {output.qc}
        """

rule clean:
    shell:
        """
        rm -f bams/*
        rm -f tables/*
        rm -f counts/*
        rm -f logs/*
        rm -f clean_reads/*
        rm -f ref/*novel_splices.txt
        rm -f post_qc/*
        rm -f pre_qc/*
        """
